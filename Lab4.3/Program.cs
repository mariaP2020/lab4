﻿using System;

namespace Lab4._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите переменную n");
            int n = Convert.ToInt32(Console.ReadLine());

            double r = MultiRec(n);

            Console.WriteLine($"R = {r}");
        }
        static double MultiRec(int n)
        {
            if (n == 11)
                return 1;
            return ((n - 11) / 3) * MultiRec(n - 1);
        }
     




    }
}
